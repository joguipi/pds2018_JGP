-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 21-Nov-2018 às 14:53
-- Versão do servidor: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bd.condominio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_fluxo`
--

CREATE TABLE `tbl_fluxo` (
  `IDFluxo` int(11) NOT NULL,
  `IDVisitante` int(11) NOT NULL,
  `IDProprietario` int(11) NOT NULL,
  `HorarioEntrada` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `HorarioSaida` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_fluxo`
--

INSERT INTO `tbl_fluxo` (`IDFluxo`, `IDVisitante`, `IDProprietario`, `HorarioEntrada`, `HorarioSaida`) VALUES
(7, 7, 5, '2018-11-02 14:57:07', '2018-11-02 14:59:07'),
(8, 8, 4, '2018-10-31 14:57:31', '2018-10-31 14:59:31'),
(9, 9, 3, '2018-11-02 14:57:41', '2018-11-02 14:59:41'),
(10, 7, 4, '2018-11-02 15:00:30', '2018-11-02 15:05:30'),
(11, 8, 3, '2018-11-02 15:00:34', '2018-11-02 15:05:30'),
(12, 9, 5, '2018-11-02 15:00:40', '2018-11-02 15:05:30'),
(13, 8, 5, '2018-11-03 01:10:45', '2018-11-03 02:35:01'),
(14, 8, 5, '2018-11-03 01:53:40', '2018-11-03 16:20:21'),
(15, 7, 3, '2018-11-03 15:44:59', '2018-11-03 16:47:50'),
(16, 7, 3, '2018-11-03 16:56:05', '2018-11-03 17:56:38'),
(17, 7, 3, '2018-11-03 16:58:32', '2018-11-03 18:59:13'),
(18, 7, 3, '2018-11-03 18:05:27', NULL),
(19, 10, 30, '2018-11-08 19:53:39', '2018-11-08 19:53:57'),
(20, 11, 3, '2018-11-08 20:18:08', '2018-11-08 20:19:03'),
(21, 11, 3, '2018-11-08 20:23:17', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_lotes`
--

CREATE TABLE `tbl_lotes` (
  `IDLote` int(11) NOT NULL,
  `Numero` int(11) NOT NULL,
  `areaConstruida` float DEFAULT NULL,
  `areaTotal` float DEFAULT NULL,
  `Rua` varchar(100) NOT NULL,
  `LinkCameraRua` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_lotes`
--

INSERT INTO `tbl_lotes` (`IDLote`, `Numero`, `areaConstruida`, `areaTotal`, `Rua`, `LinkCameraRua`) VALUES
(13, 110, 300, 500, 'Campos Salles', ''),
(14, 70, 300, 500, 'Jorge Tibiriçá', ''),
(15, 31, 300, 500, 'Benjamin Constante', ''),
(18, 77, 500, 1000, 'Pedro Mascanni', ''),
(19, 255, 300, 500, 'Rua Daniel Pedro', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_proprietarios`
--

CREATE TABLE `tbl_proprietarios` (
  `IDProprietario` int(11) NOT NULL,
  `Nome` varchar(255) NOT NULL,
  `CPF` char(11) NOT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `IDLote` int(11) NOT NULL,
  `IDVeiculo` int(11) DEFAULT NULL,
  `Telefone` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_proprietarios`
--

INSERT INTO `tbl_proprietarios` (`IDProprietario`, `Nome`, `CPF`, `Email`, `IDLote`, `IDVeiculo`, `Telefone`) VALUES
(3, 'Joãoo', '94702865374', 'joao@condominio.com', 13, 7, '1145384563'),
(4, 'Maria', '92885740850', 'maria@condominio.com', 14, 8, '11998316655'),
(5, 'Marta', '08003950856', 'marta@condominio.com', 15, 9, '1145656565'),
(30, 'Leonardo Felipe Pereira Pinto', '47399118841', 'leo@email.com', 19, 17, '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_usuarios`
--

CREATE TABLE `tbl_usuarios` (
  `IDUsuario` int(11) NOT NULL,
  `UserName` varchar(30) NOT NULL,
  `Password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_usuarios`
--

INSERT INTO `tbl_usuarios` (`IDUsuario`, `UserName`, `Password`) VALUES
(1, 'Admin', 'admin'),
(3, 'Picolo', 'qwerty');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_veiculos`
--

CREATE TABLE `tbl_veiculos` (
  `IDVeiculo` int(11) NOT NULL,
  `Placa` varchar(8) NOT NULL,
  `Modelo` varchar(250) DEFAULT NULL,
  `Tipo` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_veiculos`
--

INSERT INTO `tbl_veiculos` (`IDVeiculo`, `Placa`, `Modelo`, `Tipo`) VALUES
(7, 'ABC-1010', 'Duster', 'Carro'),
(8, 'DHG-3049', 'Celta', 'Carro'),
(9, 'AHM-1249', 'Golf', 'Carro'),
(10, 'DHZ-3147', 'Corolla', 'Automovel'),
(12, 'MNH-7041', 'Elantra', 'Carro'),
(16, 'CCD-9851', 'Palio', 'Carro'),
(17, 'ABC-1111', 'Honda Fan 2014', 'Motocicleta'),
(18, 'HAH-9090', 'Kombi Estilosa Laranja', 'Carro');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tbl_visitantes`
--

CREATE TABLE `tbl_visitantes` (
  `IDVisitante` int(11) NOT NULL,
  `Nome` varchar(255) NOT NULL,
  `CPF` char(11) NOT NULL,
  `IDVeiculo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tbl_visitantes`
--

INSERT INTO `tbl_visitantes` (`IDVisitante`, `Nome`, `CPF`, `IDVeiculo`) VALUES
(7, 'Roberto', '12696890889', 10),
(8, 'Cíntia', '36921358828', 16),
(9, 'Paulo', '31688168842', 12),
(10, 'José Picolo', '42426500867', 18),
(11, 'Weslei', '39758813056', NULL),
(12, 'gabriel', '52130995055', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_fluxos`
--
CREATE TABLE `view_fluxos` (
`IDProprietario` int(11)
,`IDVisitante` int(11)
,`IDFluxo` int(11)
,`IDLote` int(11)
,`HorarioEntrada` datetime
,`HorarioSaida` datetime
,`NomeVisitante` varchar(255)
,`IDVeiculo` int(11)
,`Placa` varchar(8)
,`Modelo` varchar(250)
,`Nome` varchar(255)
,`Rua` varchar(100)
,`Numero` int(11)
,`cpf` char(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_moradores`
--
CREATE TABLE `view_moradores` (
`Nome` varchar(255)
,`CPF` char(11)
,`Email` varchar(100)
,`Telefone` text
,`Rua` varchar(100)
,`Numero` int(11)
,`Modelo` varchar(250)
,`Placa` varchar(8)
);

-- --------------------------------------------------------

--
-- Structure for view `view_fluxos`
--
DROP TABLE IF EXISTS `view_fluxos`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_fluxos`  AS  select `p`.`IDProprietario` AS `IDProprietario`,`v`.`IDVisitante` AS `IDVisitante`,`f`.`IDFluxo` AS `IDFluxo`,`l`.`IDLote` AS `IDLote`,`f`.`HorarioEntrada` AS `HorarioEntrada`,`f`.`HorarioSaida` AS `HorarioSaida`,`v`.`Nome` AS `NomeVisitante`,`v`.`IDVeiculo` AS `IDVeiculo`,`c`.`Placa` AS `Placa`,`c`.`Modelo` AS `Modelo`,`p`.`Nome` AS `Nome`,`l`.`Rua` AS `Rua`,`l`.`Numero` AS `Numero`,`p`.`CPF` AS `cpf` from ((((`tbl_fluxo` `f` join `tbl_proprietarios` `p` on((`f`.`IDProprietario` = `p`.`IDProprietario`))) join `tbl_visitantes` `v` on((`f`.`IDVisitante` = `v`.`IDVisitante`))) left join `tbl_veiculos` `c` on((`c`.`IDVeiculo` = `f`.`IDVisitante`))) join `tbl_lotes` `l` on((`l`.`IDLote` = `p`.`IDLote`))) ;

-- --------------------------------------------------------

--
-- Structure for view `view_moradores`
--
DROP TABLE IF EXISTS `view_moradores`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_moradores`  AS  select `p`.`Nome` AS `Nome`,`p`.`CPF` AS `CPF`,`p`.`Email` AS `Email`,`p`.`Telefone` AS `Telefone`,`l`.`Rua` AS `Rua`,`l`.`Numero` AS `Numero`,`v`.`Modelo` AS `Modelo`,`v`.`Placa` AS `Placa` from ((`tbl_proprietarios` `p` join `tbl_lotes` `l` on((`p`.`IDLote` = `l`.`IDLote`))) left join `tbl_veiculos` `v` on((`p`.`IDVeiculo` = `v`.`IDVeiculo`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_fluxo`
--
ALTER TABLE `tbl_fluxo`
  ADD PRIMARY KEY (`IDFluxo`),
  ADD KEY `IDVisitante` (`IDVisitante`),
  ADD KEY `IDLote` (`IDProprietario`);

--
-- Indexes for table `tbl_lotes`
--
ALTER TABLE `tbl_lotes`
  ADD PRIMARY KEY (`IDLote`);

--
-- Indexes for table `tbl_proprietarios`
--
ALTER TABLE `tbl_proprietarios`
  ADD PRIMARY KEY (`IDProprietario`),
  ADD UNIQUE KEY `CPF` (`CPF`),
  ADD KEY `IDLote` (`IDLote`),
  ADD KEY `IDVeiculo` (`IDVeiculo`);

--
-- Indexes for table `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  ADD PRIMARY KEY (`IDUsuario`),
  ADD UNIQUE KEY `UserName` (`UserName`);

--
-- Indexes for table `tbl_veiculos`
--
ALTER TABLE `tbl_veiculos`
  ADD PRIMARY KEY (`IDVeiculo`),
  ADD UNIQUE KEY `Placa` (`Placa`);

--
-- Indexes for table `tbl_visitantes`
--
ALTER TABLE `tbl_visitantes`
  ADD PRIMARY KEY (`IDVisitante`),
  ADD UNIQUE KEY `CPF` (`CPF`),
  ADD KEY `IDVeiculo` (`IDVeiculo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_fluxo`
--
ALTER TABLE `tbl_fluxo`
  MODIFY `IDFluxo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tbl_lotes`
--
ALTER TABLE `tbl_lotes`
  MODIFY `IDLote` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_proprietarios`
--
ALTER TABLE `tbl_proprietarios`
  MODIFY `IDProprietario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `tbl_usuarios`
--
ALTER TABLE `tbl_usuarios`
  MODIFY `IDUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_veiculos`
--
ALTER TABLE `tbl_veiculos`
  MODIFY `IDVeiculo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tbl_visitantes`
--
ALTER TABLE `tbl_visitantes`
  MODIFY `IDVisitante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `tbl_fluxo`
--
ALTER TABLE `tbl_fluxo`
  ADD CONSTRAINT `tbl_fluxo_ibfk_1` FOREIGN KEY (`IDVisitante`) REFERENCES `tbl_visitantes` (`IDVisitante`),
  ADD CONSTRAINT `tbl_fluxo_ibfk_2` FOREIGN KEY (`IDProprietario`) REFERENCES `tbl_proprietarios` (`IDProprietario`);

--
-- Limitadores para a tabela `tbl_proprietarios`
--
ALTER TABLE `tbl_proprietarios`
  ADD CONSTRAINT `tbl_proprietarios_ibfk_1` FOREIGN KEY (`IDVeiculo`) REFERENCES `tbl_veiculos` (`IDVeiculo`),
  ADD CONSTRAINT `tbl_proprietarios_ibfk_2` FOREIGN KEY (`IDLote`) REFERENCES `tbl_lotes` (`IDLote`);

--
-- Limitadores para a tabela `tbl_visitantes`
--
ALTER TABLE `tbl_visitantes`
  ADD CONSTRAINT `tbl_visitantes_ibfk_1` FOREIGN KEY (`IDVeiculo`) REFERENCES `tbl_veiculos` (`IDVeiculo`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
