<?php
require_once(ABSPATH.'ado/TConnection.class.php');
require_once(ABSPATH.'ado/TTransaction.class.php');
require_once(ABSPATH.'ado/TSqlInstruction.class.php');
require_once(ABSPATH.'ado/TExpression.class.php');
require_once(ABSPATH.'ado/TFilter.class.php');
require_once(ABSPATH.'ado/TCriteria.class.php');
require_once(ABSPATH.'ado/TSqlSelect.class.php');
require_once(ABSPATH.'ado/TSqlInsert.class.php');
require_once(ABSPATH.'ado/TSqlUpdate.class.php');
require_once(ABSPATH.'ado/TSqlDelete.class.php');
require_once(ABSPATH.'ado/Log.class.php');

/**
 *  Pesquisa um Registro pelo ID em uma Tabela
 */
function find( $table, $property = null, $id = null ) {
	$table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
	$conn = TConnection::open('myconection');
	$found = null;
	try {
		if ($id) {
			$sql = new TSqlSelect;
			$sql->setEntity($table);
			$sql->addColumn(' * ');
			
			$criteria = new TCriteria;
			if(is_array($id))
			{
				$propriedade = array();
				foreach ($property as $prop)
				{
					$propriedade[] = $prop;
				}
				$i = 0;
				foreach ($id as $value)
				{
					$criteria->add(new TFilter($propriedade[$i], ' = ', $value));
					$i++;
				}
				
			}
			else
			{
				$criteria->add(new TFilter($property, ' = ', $id));
			}
			$sql->setCriteria($criteria);
			
			$result = $conn->query($sql->getInstruction());
			
			if ($result) {
				$found = array();
				while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
					$values = array();
					foreach($row as $key=>$value)
					{
						//$values[$key]=(is_string($value))? utf8_encode($value) : $value;
						$values[$key] = $value;
					}
					array_push($found, $values);
				}
			}
			
		} else {
			
			$sql = new TSqlSelect;
			$sql->setEntity($table);
			$sql->addColumn(' * ');
			
			//echo $sql->getInstruction();
			//$conn = TTransaction::get();
			$result = $conn->query($sql->getInstruction());
			
			
			if ($result) {
				$found = array();
				while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
					$values = array();
					foreach($row as $key=>$value)
					{
						//$values[$key]=(is_string($value))? utf8_encode($value) : $value;
						$values[$key] = $value;
					}
					array_push($found, $values);
				}
				
			}
		}
		//TTransaction::close();
		return $found;
	} catch (Exception $e) {
		$_SESSION['message'] = $e->GetMessage();
		$_SESSION['type'] = 'danger';
		//TTransaction::rollback();
	}
	
}

/**
 *  Pesquisa Todos os Registros de uma Tabela
 */
function find_all( $table ) {
  return find($table, null, null);
}

function find_id( $table, $property, $id){
    return find($table, $property, $id);
}

/* SAVE*/
/**
 *  Salva um Registro em uma Tabela
 */
function save($table, $array)
{
	TTransaction::open('myconection');
	$table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
	try
	{
		$sql = new TSqlInsert();
		$sql->setEntity($table);
		foreach ($array as $key=>$value)
		{
			$key = str_replace("'", "", $key);
			$sql->setRowData($key, $value);
		}
		$conn = TTransaction::get();
		//$conn->exec($sql->getInstruction());
		$conn->exec($sql->getInstruction());
		$id = $conn->lastInsertId();

		TTransaction::close();
		$_SESSION['type'] = 'success';
		
		return $id;
	}
	catch(Exception $e)
	{
		$_SESSION['message'] = $e->GetMessage();
		$_SESSION['type'] = 'danger';
		
		TTransaction::rollback();
		TTransaction::close();
	}
	
}
/* SAVE*/

/* UPDATE*/
function update($table, $array, $property, $id)
{
	$table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
	TTransaction::open('myconection');
	
	try
	{
		$sql = new TSqlUpdate();
		$sql->setEntity($table);
		$criteria = new TCriteria;
		$criteria->add(new TFilter($property, ' = ', $id));
		$sql->setCriteria($criteria);
		
		foreach ($array as $key=>$value)
		{
			$key = str_replace("'", "", $key);
			$sql->setRowData($key, $value);
		}
		$conn = TTransaction::get();
		$conn->query($sql->getInstruction());
		TTransaction::close();
		$_SESSION['type'] = 'success';
	}
	catch(Exception $e)
	{
		$_SESSION['message'] = $e->GetMessage();
		$_SESSION['type'] = 'danger';
		
		TTransaction::rollback();
		
		TTransaction::open('myconection');
		$conn = TTransaction::get();
		TTransaction::close();
	}
}
/* SAVE*/

function remove($table, $property, $id)
{
	$table = mb_convert_case($table, MB_CASE_LOWER,'UTF-8');
	TTransaction::open('myconection');
	try
	{
		$sql = new TSqlDelete();
		$sql->setEntity($table);
		$criteria = new TCriteria;
		$criteria->add(new TFilter($property, ' = ', $id));
		$sql->setCriteria($criteria);
		
		$conn = TTransaction::get();
		
		$result = $conn->query($sql->getInstruction());
		
		if($result->rowCount()==0) throw new Exception("$property inexistente");
				
		TTransaction::close();
		
		$_SESSION['type'] = 'success';
	}
	catch(Exception $e)
	{
		$_SESSION['message'] = $e->GetMessage();
		$_SESSION['type'] = 'danger';
		
		TTransaction::rollback();		
		
		TTransaction::open('myconection');
		$conn = TTransaction::get();
		TTransaction::close();
	}
}

?>