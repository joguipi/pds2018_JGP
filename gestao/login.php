<?php 

require_once 'config.php';
require_once DBAPI;

if(isset($_SESSION["id_usuario"]))
{
	header('Location: home.php');
	exit();
}

if (isset ( $_POST ['submit'] )) {
	$user = $_POST['usuario'];
	$senha = $_POST['senha'];
	$_SESSION['nomeusuario'] = $user;
	$login = find_id('tbl_usuarios', ['UserName','Password'], [$user,$senha]);
	if(count($login) > 0) {
			header ( 'Location: home.php' );
			exit ();
	} 
	else {
		header ( 'Location: index.php?erro=1' );
		exit ();
	}
	}
?>
