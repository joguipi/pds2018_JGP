<?php
include 'functions.php';

if(!isset($_SESSION['nomeusuario']) || $_SESSION['nomeusuario']!= 'Admin') {
	header ( 'Location: ../logoff.php');
}

add();

include HEADER;
?>
<!-- Título da Página -->
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Cadastrar Usuarios
		</h1>
	</div>
</div>
<!-- Insere os campos para digitação -->			
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<form action="add.php" method="post" role="form">
				<!--  Padrão para cada campo -->
				<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">UserName</label>
						<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o nome do usuário"></i>
						<input type="text" class="form-control" id="campo_nome" name="usuario[UserName]" placeholder="Nome do Usuário"  required autofocus/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Senha</label>
						<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o nome do usuário"></i>
						<input type="password" class="form-control" id="campo_novaSenha1" name="usuario[NovaSenha1]" placeholder="Digite a Nova Senha"  required autofocus/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Confirme a Nova Senha</label>
						<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o nome do usuário"></i>
						<input type="password" class="form-control" id="campo_novaSenha2" name="usuario[NovaSenha2]" placeholder="Confirma a Nova Senha" required autofocus/>
					</div>
				</div>
			</div>
				<!-- Botões -->
				<div class="row">
					<div class="col-md-6">
						<button type="submit" id="btn_submit" class="btn btn-primary btn-lg btn-block"
							style="font-size: 15px;" name="submit" >
							<i class="fa fa-check-square"></i> Cadastrar
						</button>
					</div>
	
					<div class="col-md-6">
						<a href="index.php" class="btn btn-default btn-lg btn-block"
							style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
						</a>
					</div>
				</div>
		</form>
	</div>
</div>

<?php
include FOOTER;
?>