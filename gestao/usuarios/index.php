<?php
require_once 'functions.php';
index();

if(!isset($_SESSION['nomeusuario']) || $_SESSION['nomeusuario']!= 'Admin') {
	header ( 'Location: ../logoff.php');
}

require_once HEADER;
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Usuários do Sistema
		</h1>
	</div>
</div>
			
<div class="row">
	<div class="col-md-3">
		<a href="add.php" class="btn btn-primary btn-md">
			<i class="fa fa-plus-square" aria-hidden="true"></i> Cadastrar novo Usuário
		</a>
	</div>
</div>

<br/>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Tabela com todos os usuários que tem permissão de utilizar o sistema</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<table width="100%"
					class="table table-striped table-bordered table-hover"
					id="dataTables-example">
					<thead>
						<tr>
							<th>Nome do Usuário</th>
							<th>OPÇÕES</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							if (count ($usuarios) > 0)
							{
								foreach ($usuarios as $usuario)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $usuario['UserName']?></td>
												<td>
													<a href="edit.php?id=<?php echo $usuario['IDUsuario'];?>" class="label label-warning">Editar</a>
												<?php if($usuario['UserName'] != 'Admin') {?>
										    		<a href="#" class="label label-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $usuario['IDUsuario']?>" data-id="<?php echo $usuario['IDUsuario']?>" data-usuario="<?php echo $usuario['UserName']?>" data-tipo='USUARIO'>Excluir</a>
												</td>
												<?php }?>
									</tr>
						<?php 
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php
require_once DELETEMODAL;
include FOOTER;
?>