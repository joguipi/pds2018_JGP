<?php 
require_once '../config.php';

function index()
{
	global $usuarios;
	$usuarios=find_all('tbl_usuarios');
}

function add()
{
	if(isset($_POST['submit']))
	{
		$usuario = $_POST['usuario'];
		$erro = false;
		
		if($usuario ['NovaSenha1'] == $usuario ['NovaSenha2']) {
			$usuario['Password'] = $usuario['NovaSenha1'];
			unset ( $usuario ['NovaSenha1'] );
			unset ( $usuario ['NovaSenha2'] );
		} else {
			$erro = true;
		}
		
		if ($erro == false) {
			save ( 'tbl_usuarios', $usuario );
		} else {
			$_SESSION['message'] = "Senhas diferentes. Não foi possivel Cadastrar";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');
			exit;
		}
		
		if($_SESSION['type']=="success")
		{
			$_SESSION['message']= "Usuario cadastrado com sucesso";
			header('Location: index.php');
			exit;
		}
		else
		{
			$_SESSION['message'] = "não foi possível cadastrar.";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');
			exit;
		}
	}

}

function edit($id)
{
	if(isset($id))
	{
		if(isset($_POST['submit']))
		{
			$usuario = $_POST['usuario'];
						
			$erro = false;
				// Série de verificações
				// Neste caso ocorre somente a alteração do nome do usuário.
			if ($usuario ['SenhaAnterior'] == null) {
				unset ( $usuario ['SenhaAnterior'] );
				unset ( $usuario ['NovaSenha1'] );
				unset ( $usuario ['NovaSenha2'] );
				if ($usuario ['UserName'] == 'Admin')
					$erro = true;
			} else {
				$especifUser = find_id ( 'tbl_usuarios', 'IDUsuario', $id );
				if (count ( $especifUser ) > 0) {
					if ($especifUser [0] ['Password'] == $usuario ['SenhaAnterior'] && $usuario ['NovaSenha1'] == $usuario ['NovaSenha2']) {
						$usuario['Password'] = $usuario['NovaSenha1'];
						unset ( $usuario ['SenhaAnterior'] );
						unset ( $usuario ['NovaSenha1'] );
						unset ( $usuario ['NovaSenha2'] );
					} else {
						$erro = true;
					}
				} else {
					$erro = true;
				}
			}
			
			if($erro == false) {
				update("tbl_usuarios", $usuario, "IDUsuario", $id);
			} else {
				$_SESSION['message'] = "Não foi possivel alterar a senha ou nome de usuario";
				$_SESSION['type'] = 'danger';
				header('Location: index.php');
				exit;
			}
			
			if($_SESSION['type']=="success")
			{
				$_SESSION['message']= "Perfil editado com sucesso";
				header('Location: index.php');
				exit;
			}
		}
	}
	else 
	{
		$_SESSION['message'] = "não foi possível visualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
	}
}

function delete($id)
{
	if(isset($id))
	{
		remove('tbl_usuarios', 'IDUsuario', $id);
		if($_SESSION['type']=="success")
		{
			$_SESSION['message']= "Usuario excluido com sucesso";
			header('Location: index.php');
			exit;
		}		
		else
		{
			$_SESSION['type']= "danger";
			$_SESSION['message']= "Não é possível deletar. Há usuários associados a este perfil";
			header('Location: index.php');
			exit;
		}
	}
	else
	{
		$_SESSION['message'] = "Não foi possível deletar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');
		exit;
	}
}