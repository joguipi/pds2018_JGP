			</div>
        	<!-- /#page-wrapper -->
        	
        	
		</div>
		<!-- /#wrapper -->
				
		<!-- jQuery -->
		<script src="<?php echo BASEURL; ?>vendor/jquery/jquery.js"></script>
		
		<!-- Bootstrap Core JavaScript -->
		<script src="<?php echo BASEURL; ?>vendor/bootstrap/js/bootstrap.min.js"></script>
		
		<!-- Metis Menu Plugin JavaScript -->
		<script src="<?php echo BASEURL; ?>vendor/metisMenu/metisMenu.min.js"></script>
		
		<!-- Custom Theme JavaScript -->
		<script src="<?php echo BASEURL; ?>dist/js/sb-admin-2.js"></script>
		<script src="<?php echo BASEURL; ?>js/delete.js"></script>
		
		<!-- DataTables JavaScript -->
	    <script src="<?php echo BASEURL; ?>vendor/datatables/js/jquery.dataTables.min.js"></script>
	    <script src="<?php echo BASEURL; ?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
	    <script src="<?php echo BASEURL; ?>vendor/datatables-responsive/dataTables.responsive.js"></script>
	    
	    <script type="text/javascript">
		$(function () {
			$('[data-toggle="popover"]').popover()
		});

		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		});
		$(document).ready(function() {
		    $('#dataTables-emaberto').DataTable();
		    $('#dataTables-finalizadas').DataTable();
		} );
		</script>
	</body>
</html>        