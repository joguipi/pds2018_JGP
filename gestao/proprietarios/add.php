<?php
include 'functions.php';
add ();
$lotes = find_all ( 'tbl_lotes' );
include HEADER;
?>
<!-- Título da Página -->
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">Cadastrar Proprietários</h1>
	</div>
</div>
<!-- Insere os campos para digitação -->
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<form action="add.php" method="post" role="form">

			<!--  Padrão para cada campo -->
			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Nome</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informado o nome completo"></i> <input
							type="text" class="form-control" id="campo_nome"
							name="proprietario[Nome]"
							placeholder="Digite o nome do proprietário" required autofocus />
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">CPF</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informado o CPF do proprietário"></i> <input
							type="text" class="form-control" id="campo_nome"
							name="proprietario[CPF]"
							placeholder="Digite o CPF do proprietário" required autofocus />
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Telefone</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informado o telefone do proprietário"></i> <input
							type="text" class="form-control" id="campo_nome"
							name="proprietario[Telefone]"
							placeholder="Digite o telefone do proprietário" autofocus />
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Email</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informado o email do proprietário"></i> <input
							type="text" class="form-control" id="campo_nome"
							name="proprietario[Email]"
							placeholder="Digite o email do proprietário" autofocus />
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Lote do proprietário</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser selecionado o bloco do proprietário"></i>
						<div class="form-group">
							<select class="form-control" id="sel1"
								name="proprietario[IDLote]">
							<?php
							if (count ( $lotes ) > 0) {
								foreach ( $lotes as $lote ) {
									?>
								<option value="<?php echo $lote['IDLote'];?>"> <?php echo "Rua ". $lote['Rua']. " Nº " .$lote['Numero']; ?></option>
						<?php
								}
							}
							?>
							  </select>
						</div>

					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Placa do Veículo</label> <i
							class="fa fa-1-5x fa-question-circle-o pull-right"
							data-toggle="tooltip" data-placement="left"
							title="Deve ser informado a placa do Veículo"></i> <input
							type="text" class="form-control" id="campo_nome"
							name="proprietario[Placa]"
							placeholder="Digite a placa do veículo" autofocus />
					</div>
				</div>
			</div>
			<!-- Botões -->
			<div class="row">
				<div class="col-md-6">
					<button type="submit" id="btn_submit"
						class="btn btn-primary btn-lg btn-block" style="font-size: 15px;"
						name="submit">
						<i class="fa fa-check-square"></i> Cadastrar
					</button>
				</div>

				<div class="col-md-6">
					<a href="index.php" class="btn btn-default btn-lg btn-block"
						style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
		</form>
	</div>
</div>

<?php
include FOOTER;
?>