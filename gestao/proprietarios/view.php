<?php
require_once 'functions.php';
view($_GET['id']);
require_once HEADER;
$proprietarios =  find_id('tbl_proprietarios','IDProprietario',$_GET['id']);
$carro = find('tbl_veiculos', 'IDVeiculo', $proprietarios[0]['IDVeiculo']);
$lote = find('tbl_lotes', 'IDLote', $proprietarios[0]['IDLote']);
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Informações do Proprietário
		</h1>
	</div>
</div>

<!-- Código abaixo auxilia para verificar o que está sendo retornado -->
<?php 
//	echo '<pre>';
//	print_r($lotes);
//	echo '</pre>';
?>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-12">
						<h4><strong>Detalhes:</strong></h4>
					</div>
				</div>
			</div>
			<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<p><strong>Nome: </strong><?php echo $proprietarios[0]["Nome"]?></p>
				</div>
				
				<div class="col-md-4">
					<p><strong>CPF: </strong><?php echo mask($proprietarios[0]["CPF"],'###.###.###-##')?></p>
				</div>
				
				<div class="col-md-4">
					<p><strong>Email: </strong><?php echo $proprietarios[0]["Email"]?></p>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-4">
					<p><strong>Lote: </strong><?php echo $lote[0]["Rua"] . " Nº " . $lote[0]["Numero"]?></p>
				</div>
				
				<div class="col-md-4">
					<p><strong>Placa do Veículo: </strong><?php 
					if ($proprietarios[0]['IDVeiculo'] == '') echo "Não cadastrado";
					else echo $carro[0]["Placa"]?></p>
				</div>
				
				<div class="col-md-4">
					<p><strong>Telefone: </strong><?php 
					if ($proprietarios[0]['Telefone'] == '') echo "Não cadastrado";
					else {
						if(strlen($proprietarios[0]["Telefone"]) == 11)
						echo mask($proprietarios[0]["Telefone"], '(##)#####-####');
						else 
						echo mask($proprietarios[0]["Telefone"], '(##)####-####');}
					?></p>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

<?php
require_once FOOTER;
?>