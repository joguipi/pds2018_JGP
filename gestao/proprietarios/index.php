<?php
require_once 'functions.php';
index();

require_once HEADER;
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Proprietários
		</h1>
	</div>
</div>
			
<div class="row">
	<div class="col-md-3">
		<a href="add.php" class="btn btn-primary btn-md">
			<i class="fa fa-plus-square" aria-hidden="true"></i> Adicionar novo Proprietário
		</a>
	</div>
</div>

<br/>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Tabela com todos os proprietários cadastrados no sistema</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<table width="100%"
					class="table table-striped table-bordered table-hover"
					id="dataTables-example">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Telefone</th>
							<th>OPÇÕES</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							if (count ($proprietarios) > 0)
							{
								foreach ($proprietarios as $proprietario)
								{
						?>
						<tr class="odd gradeX">
							<td><?php echo $proprietario['Nome']?></td>
							<td> <?php
							if ($proprietario['Telefone'] == '')
									echo "Não cadastrado";
								else {
									if ($proprietario["Telefone"] == null)
										echo 'Não cadastrado';
									else if (strlen ( $proprietario["Telefone"] ) == 11)
										echo mask ( $proprietario["Telefone"], '(##)#####-####' );
									else
										echo mask ( $proprietario["Telefone"], '(##)####-####' );
								}
								?></td>
							<td><a
								href="view.php?id=<?php echo $proprietario['IDProprietario'];?>"
								class="label label-primary">Ver</a> <a
								href="edit.php?id=<?php echo $proprietario['IDProprietario'];?>"
								class="label label-warning">Editar</a> <a href="#"
								class="label label-danger" data-toggle="modal"
								data-target="#deleteModal"
								id="excluir_<?php echo $proprietario['IDProprietario']?>"
								data-id="<?php echo $proprietario['IDProprietario']?>"
								data-proprietario="<?php echo $proprietario['Nome']?>"
								data-tipo='PROPRIETARIO'>Excluir</a></td>
						</tr>
						<?php 
								}
							}
						?>
					</tbody>
				</table>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<?php
require_once DELETEMODAL;
include FOOTER;
?>