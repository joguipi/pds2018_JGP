<?php
require_once '../config.php';

function relatorio($tipo)
{
    if(isset($_POST['submit']))
    {        
        global $relatorios;
        $fields = $_POST['cpf'];
        $relatorios = array();
        $fields = preg_replace ( "/[^0-9]/", "", $fields);
                
        if($tipo=="entradasaida")
        {
        	$relatorios = find_id("view_fluxos", "cpf", $fields);
        }
    }
}

function relatorio_moradores()
{
		global $relatorio_morador;
		$relatorio_morador= array();
		$relatorio_morador= find_all('view_moradores');
}

function mask($val, $mask) {
	$maskared = '';
	$k = 0;
	for($i = 0; $i <= strlen ( $mask ) - 1; $i ++) {
		if ($mask [$i] == '#') {
			if (isset ( $val [$k] ))
				$maskared .= $val [$k ++];
		} else {
			if (isset ( $mask [$i] ))
				$maskared .= $mask [$i];
		}
	}
	return $maskared;
}
?>