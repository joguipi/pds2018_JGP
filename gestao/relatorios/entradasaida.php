<?php
require_once 'functions.php';
relatorio ("entradasaida");

include HEADER;

if (! isset ( $_POST ['submit'] )) {
	
	?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header text-center">
			<span>Relatório de Entrada e Saída de Visitantes</span>
		</h1>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<form action="entradasaida.php" method="post">
			<div class="row">
				<div class="col-md-12">
					<div id="div_campo_data_inicio" class="form-group has-feedback">
						<div class="row">
							<div class="col-md-12">
								<label for="campo_data_inicio">CPF do Proprietário</label> <i
									class="" id="icon_campo_data_inicio"></i> <i
									class="fa fa-1-5x fa-question-circle-o pull-right"
									data-toggle="tooltip" data-placement="left"
									title="Deve ser informado a data de início"></i>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<input type="text" class="form-control" id="campo_data_inicio"
									name="cpf" required oninput="verificar_data_inicio();" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<button type="submit" name="submit"
						class="btn btn-primary btn-block">
						<i class="fa fa-check-square"></i> Pesquisar
					</button>

				</div>

			</div>
		</form>
	</div>
</div>

<?php
} else {
?>
<div class="row">
	<div class="col-md-12">
		<div class="card mb-3">
			<div class="card-header">
				<div class="row" style="text-align: center;">
					<div class="col-md-12">
						<h3>Relatório de Entrada e Saída</h3>
						<?php 
						if (count($relatorios) > 0) {
						?>
						<p>
							<strong>Nome do Proprietário: </strong> <?php echo $relatorios[0]['Nome']?></p>
						<p>
							<strong>Rua: </strong> <?php  echo $relatorios[0]['Rua']?></p>
						<p>
							<strong>Número: </strong><?php echo $relatorios[0]['Numero']?></p>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class=" table-bordered" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th style="border: 1px solid #ccc; padding: 7px">Nome do
									Visitante</th>
								<th style="border: 1px solid #ccc; padding: 7px">Horário de
									Entrada</th>
								<th style="border: 1px solid #ccc; padding: 7px">Placa do Carro</th>
								<th style="border: 1px solid #ccc; padding: 7px">Modelo</th>
								<th style="border: 1px solid #ccc; padding: 7px">Horário de
									Saída</th>
							</tr>
						</thead>
						<tbody>
                	 <?php
	foreach ( $relatorios as $r ) {
		$date = new DateTime($r['HorarioEntrada']);
		$date2 = new DateTime($r['HorarioSaida']);
		
		?>
							<tr>
								<td style="border: 1px solid #ccc; padding: 7px"><?php echo $r['NomeVisitante']?></td>
								<td style="border: 1px solid #ccc; padding: 7px"><?php echo $date->format('d/m/Y H:i:s');?></td>
								<td style="border: 1px solid #ccc; padding: 7px"><?php 
									if($r['Placa'] == null) echo 'Não cadastrado';
									else echo $r['Placa'];?></td>
								<td style="border: 1px solid #ccc; padding: 7px"><?php
									if($r['Modelo'] == null) echo 'Não cadastrado';
									else echo $r['Modelo'];?></td>
								<td style="border: 1px solid #ccc; padding: 7px"><?php 
								 if ($r['HorarioSaida'] == null) echo "Não há registros"; 
								 else echo $date2->format('d/m/Y H:i:s');?></td>
							</tr>
									
						<?php
	}
	?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<div class="col-md-4 offset-md-4">
	<a href="entradasaida.php"
		class="btn btn-primary btn-block nao_imprimir"> <i
		class="fa fa-arrow-left"></i> Voltar
	</a>
</div>
<?php
} else {
?>
	<div class="col-md-6 col-md-offset-3">
	<label>Nenhum Registro Encontrado.</label>
	<a href="entradasaida.php"
		class="btn btn-primary btn-block nao_imprimir"> <i
		class="fa fa-arrow-left"></i> Voltar
	</a>
</div>
<?php 
}
}
?>