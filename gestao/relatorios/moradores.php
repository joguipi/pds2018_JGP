<?php
require_once 'functions.php';
relatorio_moradores();

include HEADER;
?>

<div class="row">
	<div class="col-md-12">
		<div class="card mb-3">
			<div class="card-header">
				<div class="row" style="text-align: center;">
					<div class="col-md-12">
						<h3>Relatório de Moradores</h3>
						<?php 
						if (count($relatorio_morador) > 0) {
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
			
			<div class="card-body">
				<div class="table-responsive">
					<table class=" table-bordered" width="100%" cellspacing="0">
						<thead>
							<tr>
								<th style="border: 1px solid #ccc; padding: 7px">Nome do Morador</th>
								<th style="border: 1px solid #ccc; padding: 7px">CPF</th>
								<th style="border: 1px solid #ccc; padding: 7px">Email</th>
								<th style="border: 1px solid #ccc; padding: 7px">Telefone</th>
								<th style="border: 1px solid #ccc; padding: 7px">Rua</th>
								<th style="border: 1px solid #ccc; padding: 7px">Número</th>
								<th style="border: 1px solid #ccc; padding: 7px">Modelo</th>
								<th style="border: 1px solid #ccc; padding: 7px">Placa</th>
							</tr>
						</thead>
						<tbody>
                	 <?php
        foreach ( $relatorio_morador as $r ) {		
		?>
							<tr>
								<td style="border: 1px solid #ccc; padding: 7px"><?php echo $r['Nome']?></td>
								<td style="border: 1px solid #ccc; padding: 7px"><?php echo mask($r["CPF"],'###.###.###-##');?></td>
								<td style="border: 1px solid #ccc; padding: 7px"><?php echo $r['Email']?></td>
								<td style="border: 1px solid #ccc; padding: 7px"><?php
								if ($r['Telefone'] == '') echo "Não cadastrado";
								else {
									if(strlen($r["Telefone"]) == 11)
										echo mask($r["Telefone"], '(##) #####-####');
										else
											echo mask($r["Telefone"], '(##) ####-####');}?></td>
								<td style="border: 1px solid #ccc; padding: 7px"><?php echo $r['Rua']?></td>
								<td style="border: 1px solid #ccc; padding: 7px"><?php echo $r['Numero']?></td>
								<td style="border: 1px solid #ccc; padding: 7px"><?php
									if($r['Modelo'] == null) echo 'Não cadastrado';
									else echo $r['Modelo'];?></td>
								<td style="border: 1px solid #ccc; padding: 7px"><?php 
								 if ($r['Placa'] == null) echo "Não cadastrado"; 
								 else echo $r['Placa'];?></td>
							</tr>
						<?php
	}
	?>
						</tbody>
					</table>
				</div>
			</div>
<br>
<div class="col-md-4 offset-md-4">
	<a href="moradores.php"
		class="btn btn-primary btn-block nao_imprimir"> <i
		class="fa fa-arrow-left"></i> Voltar
	</a>
</div>
<?php
} else {
?>
	<div class="col-md-6 col-md-offset-3">
	<label>Nenhum Registro Encontrado.</label>
	<a href="entradasaida.php"
		class="btn btn-primary btn-block nao_imprimir"> <i
		class="fa fa-arrow-left"></i> Voltar
	</a>
</div>
<?php 
}
?>