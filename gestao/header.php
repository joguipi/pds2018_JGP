<?php
if(!isset($_SESSION['nomeusuario'])) {
	header ( "Location:" . BASEURL . "index.php?erro=1" );
}

$conn = TConnection::open('myconection');

?>

<!DOCTYPE html>
<html lang="pt-br">


	<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Gestão da Orthocenter">

<title>Picolo</title>

<link rel="icon" href="<?=BASEURL ?>/gestao/ico.png" type="image/png"/>
<link rel="shortcut icon" href="<?=BASEURL ?>/gestao/ico.png" type="image/png"/>

<!-- Bootstrap Core CSS -->
<link href="<?php echo BASEURL; ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<?php echo BASEURL; ?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->


<!-- Morris Charts CSS -->
<link href="<?php echo BASEURL; ?>vendor/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?php echo BASEURL; ?>vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">

<!-- DataTables CSS -->
<link href="<?php echo BASEURL; ?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="<?php echo BASEURL; ?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
<!-- Social Buttons CSS -->
<link href="<?php echo BASEURL; ?>vendor/bootstrap-social/bootstrap-social.css" rel="stylesheet">

<link href="<?php echo BASEURL; ?>dist/css/sb-admin-2.css" rel="stylesheet">
</head>

<body>
	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?php echo BASEURL;?>gestao/home.php"><i class="fa fa-home"></i> Sistema de Gerenciamente de Condomínio</a>
			</div>
			<!-- /.navbar-header -->
		
			<ul class="nav navbar-top-links navbar-right">
				<li class="dropdown pull-right"><a class="dropdown-toggle"
					data-toggle="dropdown" href="#"><i class="fa fa-user fa-fw"></i><?php echo $_SESSION['nomeusuario']?> <i
						class="fa fa-caret-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="<?php echo BASEURL?>gestao/logoff.php"><span class="glyphicon glyphicon-off"></span> Sair</a></li>
					</ul> <!-- /.dropdown-user --></li>
				<!-- /.dropdown -->
			</ul>
			<!-- /.navbar-top-links -->
			

			<div class="navbar-default sidebar" role="navigation">
				<div class="sidebar-nav navbar-collapse collapse">
					<ul class="nav" id="side-menu">
						
								<li>
									<a href="<?php echo BASEURL; ?>gestao/lotes/">
										<i class="fa fa-pagelines fa-fw"></i> Lotes
									</a>
								</li>
						
					
								<li>
									<a href="<?php echo BASEURL; ?>gestao/proprietarios/">
										<i class="fa fa-address-card-o fa-fw"></i> Proprietários
									</a>
								</li>                            		
				
								<li>
									<a href="<?php echo BASEURL; ?>gestao/veiculos/">
										<i class="fa fa-car fa-fw"></i> Veículos
									</a>
								</li>
					
								<li>
									<a href="<?php echo BASEURL; ?>gestao/fluxo/">
										<i class="fa fa-exchange fa-fw"></i> Entrada e Saída
									</a>
								</li>
					
								<li>
									<a href="<?php echo BASEURL; ?>gestao/visitantes/">
										<i class="fa fa-users fa-fw"></i> Visitantes
									</a>
								</li>
								
								<li>
									<a href="#">
										<i class="fa fa-list-alt fa-fw"> </i> Relatórios<span class="fa arrow"></span>
									</a>
									<ul class="nav nav-second-level">
								
										<li>
											<a href="<?php echo BASEURL; ?>gestao/relatorios/entradasaida.php">
												Entrada e Saída 
											</a>
										</li>
								
										<li>
											<a href="<?php echo BASEURL; ?>gestao/relatorios/moradores.php">
												 Moradores
											</a>
										</li>
									</ul> <!-- /.nav-second-level -->
									
								<?php if($_SESSION['nomeusuario'] == "Admin") { ?>
								<li><a href="<?php echo BASEURL; ?>gestao/usuarios/"> <i
										class="fa fa fa-key fa-fw"></i> Usuários
								</a></li>
								<?php  }?>
								</li>
								

					
					</ul>
				</div>
				<!-- /.sidebar-collapse -->
			</div>
			<!-- /.navbar-static-side -->
		</nav>
		<div id="page-wrapper">
				
			<?php
			if (isset ( $_SESSION ['message'] )) {
				echo '<div class="row">';
				echo '<div class="col-md-4 col-md-offset-4">';
				echo '<div class="alert alert-' . $_SESSION ['type'] . '" id="feedback-' . $_SESSION ['type'] . '" role="alert">';
				// echo '<strong>'.$_SESSION['type'].'</strong> '.$_SESSION['message'];
				echo $_SESSION ['message'];
				// if($_SESSION['type']!='success')
				// {
				echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
				// }
				echo '</div>';
				echo '</div>';
				echo '</div>';
				
				unset ( $_SESSION ['message'] );
				unset ( $_SESSION ['type'] );
			}
			?>
			