<?php
session_start();
if(isset($_SESSION['id_usuario']))
{
	header('Location: home.php');
}
if(isset($_COOKIE["id_usuario"]))
{
	if($_COOKIE["id_usuario"]!="deleted")
	{
		header('Location:login.php');
		exit();
	}
}
$erro = isset($_GET['erro']) ? $_GET['erro'] : 0 ;
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <title>Condomínio Picolo</title>

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Digite seu usuário e senha</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="login.php" method="post" autocomplete="off">
                            <fieldset>
	                            <div class="row">
	                            	<div class="col-xs-12">
	                            		<div class="form-group">
		                                    <input class="form-control" id="user" placeholder="Usuário" name="usuario" type="text" autofocus>
		                                </div>
	                            	</div>
	                            </div>
                                
                                <div class="row">
	                            	<div class="col-xs-12">
	                            		<div class="form-group has-feedback">
		                                    <input class="form-control" id="pass" placeholder="Senha" name="senha" type="password" value="">
		                                </div>
	                            	</div>
	                            </div>
                                
                                <div class="row">
	                            	<div class="col-xs-12">
	                            		<div class="checkbox">
		                                    <label>
		                                        <input name="remember" type="checkbox" value="Remember Me">Lembrar
		                                    </label>
		                                </div>
	                            	</div>
	                            </div>
                               
                               	 <div class="row" style="margin-left:127px;">
										<button type="submit" class="btn btn-primary" name="submit" id="submit">Acessar</button>
									 </div>
								<?php
                        if ($erro == 1){
                            echo '<font color="red"> Usuário e ou senha Inválido(s)!</font>';
                        }
                    ?> 
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    
    <script src="../js/show_pass.js"></script>

</body>
</html>