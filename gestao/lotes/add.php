<?php
include 'functions.php';
add();

include HEADER;
?>
<!-- Título da Página -->
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Cadastrar Lotes
		</h1>
	</div>
</div>
<!-- Insere os campos para digitação -->			
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<form action="add.php" method="post" role="form">
				<!--  Padrão para cada campo -->
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Rua</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado a rua"></i>
							<input type="text" class="form-control" id="campo_nome" name="lote[Rua]" placeholder="Digite o nome da Rua" required autofocus/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Número</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o número do lote"></i>
							<input type="number" class="form-control" id="campo_nome" name="lote[Numero]" placeholder="Digite o número do lote" required autofocus/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Area Construída</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado a área construída em m²"></i>
							<input type="number" class="form-control" id="campo_nome" name="lote[areaConstruida]" placeholder="Digite a área construída" autofocus/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Area Total</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado a área total em m²"></i>
							<input type="number" class="form-control" id="campo_nome" name="lote[areaTotal]" placeholder="Digite a área total do lote" autofocus/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Link</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o link para a câmera 24h."></i>
							<input type="text" class="form-control" id="campo_nome" name="lote[LinkCameraRua]" placeholder="Digite o link da câmera da rua" autofocus/>
						</div>
					</div>
				</div>
				<!-- Botões -->
				<div class="row">
					<div class="col-md-6">
						<button type="submit" id="btn_submit" class="btn btn-primary btn-lg btn-block"
							style="font-size: 15px;" name="submit" >
							<i class="fa fa-check-square"></i> Cadastrar
						</button>
					</div>
	
					<div class="col-md-6">
						<a href="index.php" class="btn btn-default btn-lg btn-block"
							style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
						</a>
					</div>
				</div>
		</form>
	</div>
</div>

<?php
include FOOTER;
?>