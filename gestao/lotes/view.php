<?php
require_once 'functions.php';
view($_GET['id']);
require_once HEADER;
$lotes =  find_id('tbl_lotes','IDLote',$_GET['id'])
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Informações do Lote
		</h1>
	</div>
</div>

<!-- Código abaixo auxilia para verificar o que está sendo retornado -->
<?php 
//	echo '<pre>';
//	print_r($lotes);
//	echo '</pre>';
?>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-12">
						<h4><strong>Detalhes:</strong></h4>
					</div>
				</div>
			</div>
			<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<p><strong>Rua: </strong><?php echo $lotes[0]["Rua"]?></p>
				</div>
				
				<div class="col-md-6">
					<p><strong>Número: </strong><?php echo $lotes[0]["Numero"]?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<p><strong>Área Total: </strong><?php echo $lotes[0]["areaConstruida"]?></p>
				</div>
				
				<div class="col-md-6">
					<p><strong>Área Construída: </strong><?php echo $lotes[0]["areaTotal"]?></p>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<p><strong>Link Câmera: </strong><?php echo $lotes[0]["LinkCameraRua"]?></p>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

<?php
require_once FOOTER;
?>