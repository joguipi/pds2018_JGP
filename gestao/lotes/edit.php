<?php
include 'functions.php';


edit($_GET['id']);

include HEADER;

$lote = find_id("tbl_lotes", "idLote" , $_GET['id']);
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Editar Lote
		</h1>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<form action="edit.php?id=<?php echo $_GET['id'];?>" method="post" >
			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Rua</label>
						<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado a rua"></i>
						<input type="text" class="form-control" id="campo_nome" name="lote[Rua]" placeholder="Nome da Rua" value="<?php echo $lote[0]['Rua']?>" required autofocus/>
					</div>
				</div>
			</div>
									
			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Número</label>
						<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o numero"></i>
						<input type="number" class="form-control" id="campo_nome" name="lote[Numero]" placeholder="Número do lote" value="<?php echo $lote[0]['Numero']?>" required autofocus/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Area Construída</label>
						<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado a área construída em m²"></i>
						<input type="number" class="form-control" id="campo_nome" name="lote[areaConstruida]" placeholder="Tamanho do Lote" value="<?php echo $lote[0]['areaConstruida']?>" autofocus/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Area Total</label>
						<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado a área construída em m²"></i>
						<input type="number" class="form-control" id="campo_nome" name="lote[areaTotal]" placeholder="Tamanho do Lote" value="<?php echo $lote[0]['areaTotal']?>" autofocus/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-xs-12">
					<div id="div_campo_nome" class="form-group has-feedback">
						<label for="campo_nome">Link</label>
						<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado a área construída em m²"></i>
						<input type="text" class="form-control" id="campo_nome" name="lote[LinkCameraRua]" placeholder="LInk" value="<?php echo $lote[0]['LinkCameraRua']?>" autofocus/>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6">
					<button type="submit" id="btn_submit" class="btn btn-primary btn-lg btn-block"
						style="font-size: 15px;" name="submit" >
					<i class="fa fa-check-square"></i> Editar
				</button>
			</div>
			
			<div class="col-md-6">
				<a href="index.php" class="btn btn-default btn-lg btn-block"
					style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- /.row (nested) -->

<?php
include FOOTER;
?>