<?php 
require_once '../config.php';

function index()
{
	global $lotes;
	$lotes= find_all('TBL_LOTES');
}

function view($id)
{
	if(isset($id))
	{
		global $lote;
		$result = find_id('TBL_LOTES','IDLote',$id);
		if(count($result)==0)
		{
			$_SESSION['message'] = "Não foi possível visualizar: ID não encontrado";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');//
			exit;
		}
		$perfil = $result[0];
	}
	else
	{
		$_SESSION['message'] = "Não foi possível visualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
		
	}
}

function add()
{
	if(isset($_POST['submit']))
	{
		$lote = $_POST['lote'];
		
		save('TBL_LOTES', $lote);
		
		if($_SESSION['type']=="success")
		{
			$_SESSION['message']= "Lote cadastrado com sucesso";
			header('Location: index.php');
			exit;
		}
		else
		{
			$_SESSION['message'] = "não foi possível cadastrar.";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');
			exit;
		}
	}

}

function edit($id)//fazer edit_perfil
{
	if(isset($id))
	{
		if(isset($_POST['submit']))
		{
			
			$lote = $_POST['lote'];
			
			update("tbl_lotes", $lote, "IDLote", $id);
			
			if($_SESSION['type']=="success")
			{
				$_SESSION['message']= "Perfil editado com sucesso";
				header('Location: index.php');
				exit;
			}
		}
		else
		{
			view($id);
		}
	}
	else 
	{
		$_SESSION['message'] = "não foi possível visualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
	}
}

function delete($id)
{
	if(isset($id))
	{
		remove('TBL_LOTES', 'IDLote', $id);
		if($_SESSION['type']=="success")
		{
			$_SESSION['message']= "Lote excluido com sucesso";
			header('Location: index.php');
			exit;
		}		
		else
		{
			$_SESSION['type']= "danger";
			$_SESSION['message']= "Não é possível deletar. Há usuários associados a este perfil";
			header('Location: index.php');
			exit;
		}
	}
	else
	{
		$_SESSION['message'] = "Não foi possível deletar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');
		exit;
	}
}