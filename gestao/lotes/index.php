<?php
require_once 'functions.php';
index();

if(!isset($_SESSION['nomeusuario'])) {
	header ( 'Location: ../logoff.php');
}

require_once HEADER;
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Lotes
		</h1>
	</div>
</div>
			
<div class="row">
	<div class="col-md-3">
		<a href="add.php" class="btn btn-primary btn-md">
			<i class="fa fa-plus-square" aria-hidden="true"></i> Adicionar novo Lote
		</a>
	</div>
</div>

<br/>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Tabela com todos os lotes cadastrados no sistema</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<table width="100%"
					class="table table-striped table-bordered table-hover"
					id="dataTables-example">
					<thead>
						<tr>
							<th>Rua</th>
							<th>Número</th>
							<th>OPÇÕES</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							if (count ($lotes ) > 0)
							{
								foreach ($lotes as $lote)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $lote['Rua']?></td>
										<td> <?php echo $lote['Numero']?> </td>
												<td>
													<a href="view.php?id=<?php echo $lote['IDLote'];?>" class="label label-primary">Ver</a>
													<a href="edit.php?id=<?php echo $lote['IDLote'];?>" class="label label-warning">Editar</a>
										    		<a href="#" class="label label-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $lote['IDLote']?>" data-id="<?php echo $lote['IDLote']?>" data-lote="<?php echo $lote['Rua']?>" data-tipo='LOTE'>Excluir</a>
												</td>
									</tr>
						<?php 
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<?php
require_once DELETEMODAL;
include FOOTER;
?>