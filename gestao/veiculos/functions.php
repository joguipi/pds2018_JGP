<?php 
require_once '../config.php';

function index()
{
	global $veiculos;
	$veiculos= find_all('TBL_VEICULOS');
}

function view($id)
{
	if(isset($id))
	{
		global $veiculo;
		$result = find_id('TBL_VEICULOS','IDVeiculo',$id);
		if(count($result)==0)
		{
			$_SESSION['message'] = "Não foi possível visualizar: ID não encontrado";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');//
			exit;
		}
		$perfil = $result[0];
	}
	else
	{
		$_SESSION['message'] = "Não foi possível visualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
	}
}

function add()
{
	if(isset($_POST['submit']))
	{
		$veiculo= $_POST['veiculo'];
		
		save('TBL_VEICULOS', $veiculo);
		
		if($_SESSION['type']=="success")
		{
			$_SESSION['message']= "Veículo cadastrado com sucesso";
			header('Location: index.php');
			exit;
		}	
		else
		{
			$_SESSION['message'] = "Não foi possível visualizar: ID não especificado";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');//
			exit;
		}
	}

}

function edit($id)//fazer edit_perfil
{
	if(isset($id))
	{
		if(isset($_POST['submit']))
		{
			
			$veiculo = $_POST['veiculo'];
			
			update("tbl_veiculos", $veiculo, "IDVeiculo", $id);
			
			if($_SESSION['type']=="success")
			{
				$_SESSION['message']= "Veículo editado com sucesso";
				header('Location: index.php');
				exit;
			}
		}
		else
		{
			view($id);
		}
	}
	else 
	{
		$_SESSION['message'] = "Não foi possível visualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
	}
}

function delete($id)
{
	if(isset($id))
	{
		remove('TBL_VEICULOS', 'IDVeiculo', $id);
		if($_SESSION['type']=="success")
		{
			$_SESSION['message']= "Veículo excluído com sucesso";
			header('Location: index.php');
			exit;
		}		
		else
		{
			$_SESSION['type']= "danger";
			$_SESSION['message']= "Não é possível deletar. Há usuários associados a este perfil";
			header('Location: index.php');
			exit;
		}
	}
	else
	{
		$_SESSION['message'] = "Não foi possível deletar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');
		exit;
	}
}