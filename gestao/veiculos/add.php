<?php
include 'functions.php';
add();

include HEADER;
?>
<!-- Título da Página -->
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Cadastrar Veículos
		</h1>
	</div>
</div>
<!-- Insere os campos para digitação -->			
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<form action="add.php" method="post" role="form">
				<!--  Padrão para cada campo -->
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Placa</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado a placa do veículo"></i>
							<input type="text" class="form-control" id="campo_nome" name="veiculo[Placa]" placeholder="Digite a placa do veículo" required autofocus/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Modelo</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o modelo do veículo"></i>
							<input type="text" class="form-control" id="campo_nome" name="veiculo[Modelo]" placeholder="Digite o modelo do veículo" autofocus/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Tipo</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o tipo do veículo"></i>
							<div class="form-group">
							  <select class="form-control" id="sel1" name="veiculo[Tipo]">
							    <option value="Carro">Carro</option>
							    <option value="Motocicleta">Motocicleta</option>
							    <option value="Caminhão">Caminhão</option>
							  </select>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Botões -->
				<div class="row">
					<div class="col-md-6">
						<button type="submit" id="btn_submit" class="btn btn-primary btn-lg btn-block"
							style="font-size: 15px;" name="submit" >
							<i class="fa fa-check-square"></i> Cadastrar
						</button>
					</div>
	
					<div class="col-md-6">
						<a href="index.php" class="btn btn-default btn-lg btn-block"
							style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
						</a>
					</div>
				</div>
		</form>
	</div>
</div>

<?php
include FOOTER;
?>