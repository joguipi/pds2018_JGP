<?php
include 'functions.php';


edit($_GET['id']);

include HEADER;

$veiculo = find_id("tbl_veiculos", "IDVeiculo" , $_GET['id']);
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Editar Veículo
		</h1>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<form action="edit.php?id=<?php echo $_GET['id'];?>" method="post" >
		<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Placa</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado a placa do veículo"></i>
							<input type="text" class="form-control" id="campo_nome" name="veiculo[Placa]" placeholder="Placa do Carro" value="<?php echo $veiculo[0]['Placa']?>" required autofocus/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Modelo</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o modelo do veículo"></i>
							<input type="text" class="form-control" id="campo_nome" name="veiculo[Modelo]" placeholder="Modelo do veículo" value="<?php echo $veiculo[0]['Modelo']?>" autofocus/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Tipo</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o tipo do veículo"></i>
							<input type="text" class="form-control" id="campo_nome" name="veiculo[Tipo]" placeholder="Tipo de Veículo" value="<?php echo $veiculo[0]['Tipo']?>" autofocus/>
						</div>
					</div>
				</div>
			
			<div class="row">
				<div class="col-md-6">
					<button type="submit" id="btn_submit" class="btn btn-primary btn-lg btn-block"
						style="font-size: 15px;" name="submit" >
					<i class="fa fa-check-square"></i> Editar
				</button>
			</div>
			
			<div class="col-md-6">
				<a href="index.php" class="btn btn-default btn-lg btn-block"
					style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- /.row (nested) -->

<?php
include FOOTER;
?>