<?php
require_once 'functions.php';
index();

require_once HEADER;
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Veículos
		</h1>
	</div>
</div>
			
<div class="row">
	<div class="col-md-3">
		<a href="add.php" class="btn btn-primary btn-md">
			<i class="fa fa-plus-square" aria-hidden="true"></i> Cadastrar novo veículo
		</a>
	</div>
</div>

<br/>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Tabela com todos os veículos que possuem autorização para entrar no condomínio</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<table width="100%"
					class="table table-striped table-bordered table-hover"
					id="dataTables-example">
					<thead>
						<tr>
							<th>Placa</th>
							<th>Modelo</th>
							<th>OPÇÕES</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							if (count ($veiculos ) > 0)
							{
								foreach ($veiculos as $veiculo)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $veiculo['Placa']?></td>
										<td> <?php echo $veiculo['Modelo']?> </td>
												<td>
													<a href="view.php?id=<?php echo $veiculo['IDVeiculo'];?>" class="label label-primary">Ver</a>
													<a href="edit.php?id=<?php echo $veiculo['IDVeiculo'];?>" class="label label-warning">Editar</a>
										    		<a href="#" class="label label-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $veiculo['IDVeiculo']?>" data-id="<?php echo $veiculo['IDVeiculo']?>" data-veiculo="<?php echo $veiculo['Placa']?>" data-tipo='VEICULO'>Excluir</a>
												</td>
									</tr>
						<?php 
								}
							}
						?>
					</tbody>
				</table>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<?php
require_once DELETEMODAL;
include FOOTER;
?>