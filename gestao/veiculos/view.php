<?php
require_once 'functions.php';
view($_GET['id']);
require_once HEADER;
$veiculo =  find_id('tbl_veiculos','IDVeiculo',$_GET['id'])
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Informações do Veículo
		</h1>
	</div>
</div>

<!-- Código abaixo auxilia para verificar o que está sendo retornado -->
<?php 
//	echo '<pre>';
//	print_r($lotes);
//	echo '</pre>';
?>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-12">
						<h4><strong>Detalhes:</strong></h4>
					</div>
				</div>
			</div>
			<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					<p><strong>Placa: </strong><?php echo $veiculo[0]["Placa"]?></p>
				</div>
				
				<div class="col-md-6">
					<p><strong>Modelo: </strong><?php echo $veiculo[0]["Modelo"]?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<p><strong>Tipo: </strong><?php echo $veiculo[0]["Tipo"]?></p>
				</div>
			</div>
			
			</div>
		</div>
	</div>
</div>

<?php
require_once FOOTER;
?>