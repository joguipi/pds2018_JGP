<?php 
require_once '../config.php';

function index()
{
	global $fluxos;
	$fluxos= find_all('TBL_FLUXO');
}

function view($id)
{
	if(isset($id))
	{
		global $lote;
		$result = find_id('TBL_FLUXO','IDFluxo',$id);
		if(count($result)==0)
		{
			$_SESSION['message'] = "Não foi possível visualizar: ID não encontrado";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');//
			exit;
		}
		$perfil = $result[0];
	}
	else
	{
		$_SESSION['message'] = "Não foi possível visualizar: ID não especificado";
		$_SESSION['type'] = 'danger';
		header('Location: index.php');//
		exit;
		
	}
}

function add()
{
	if(isset($_POST['submit']))
	{
		$fluxo = $_POST['fluxo'];
		
		// Verifica se o CPF é válido.
		if (validaCPF ( $fluxo['CPF'] ) == false) {
			$_SESSION ['message'] = "CPF Inválido";
			$_SESSION ['type'] = 'danger';
			header ( 'Location: index.php' );
			exit ();
		}
		
		$cpfOk = preg_replace ( "/[^0-9]/", "", $fluxo['CPF']);
		
		// Procura pelo visitante através do cpf
		$visitante = find('tbl_visitantes', 'CPF', $cpfOk);
		if (count ( $visitante) > 0) {
			$fluxo['IDVisitante'] = $visitante[0]['IDVisitante'];
		} else {
			$_SESSION ['message'] = "Visitante não encontrado";
			$_SESSION ['type'] = 'danger';
			header ( 'Location: index.php' );
			exit ();
		}
		//Remove CPF
		unset ($fluxo['CPF']);
		
		save('TBL_FLUXO', $fluxo);
		
		if($_SESSION['type']=="success")
		{
			$_SESSION['message']= "Registro Entrada cadastrado com sucesso";
			header('Location: index.php');
			exit;
		}
		else
		{
			$_SESSION['message'] = "não foi possível cadastrar.";
			$_SESSION['type'] = 'danger';
			header('Location: index.php');
			exit;
		}
	}
}

function updateGoOut($id) {
	$fluxo = $_POST ['fluxo'];
	
	date_default_timezone_set("America/Sao_Paulo");
	$dt = new DateTime();	
	$fluxo ['HorarioSaida'] = $dt->format('Y-m-d H:i:s');
	
	update ( "tbl_fluxo", $fluxo, "IDFluxo", $id );
	
	if ($_SESSION ['type'] == "success") {
		$_SESSION ['message'] = "Registro editado com sucesso";
		header ( 'Location: index.php' );
		exit ();
	}
}

function validaCPF($cpf = null) {
	
	// Verifica se um número foi informado
	if (empty ( $cpf )) {
		return false;
	}
	
	// Elimina possivel mascara
	$cpf = preg_replace ( "/[^0-9]/", "", $cpf );
	$cpf = str_pad ( $cpf, 11, '0', STR_PAD_LEFT );
	
	// Verifica se o numero de digitos informados é igual a 11
	if (strlen ( $cpf ) != 11) {
		return false;
	} // Verifica se nenhuma das sequências invalidas abaixo
	// foi digitada. Caso afirmativo, retorna falso
	else if ($cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
		return false;
		// Calcula os digitos verificadores para verificar se o
		// CPF é válido
	} else {
		
		for($t = 9; $t < 11; $t ++) {
			
			for($d = 0, $c = 0; $c < $t; $c ++) {
				$d += $cpf {$c} * (($t + 1) - $c);
			}
			$d = ((10 * $d) % 11) % 10;
			if ($cpf {$c} != $d) {
				return false;
			}
		}
		
		return true;
	}
}