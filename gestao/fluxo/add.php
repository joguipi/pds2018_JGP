<?php
include 'functions.php';
add();
$proprietarios = find_all('tbl_proprietarios');
include HEADER;
?>
<!-- Título da Página -->
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Registrar Entrada
		</h1>
	</div>
</div>
<!-- Insere os campos para digitação -->			
<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<form action="add.php" method="post" role="form">
				<!--  Padrão para cada campo -->
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">CPF Visitante</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o CPF do Visitante"></i>
							<input type="text" class="form-control" id="campo_nome" name="fluxo[CPF]" placeholder="CPF do Visitante" required autofocus/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Proprietário</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser selecionado o Proprietário"></i>
							
							<div class="form-group">
							<select class="form-control" id="sel1"
								name="fluxo[IDProprietario]">
							<?php
							if (count ( $proprietarios) > 0) {
								foreach ( $proprietarios as $p ) {
									$lote = find_id('tbl_lotes', 'IDLote', $p['IDLote']);
									?>
								<option value="<?php echo $p['IDProprietario'];?>"> <?php echo $p['Nome']. " - Rua ". $lote[0]['Rua']. " Nº " .$lote[0]['Numero']; ?></option>
						<?php
								}
							}
							?>
							  </select>
						</div>
							
						</div>
					</div>
				</div>
				
				<!-- Botões -->
				<div class="row">
					<div class="col-md-6">
						<button type="submit" id="btn_submit" class="btn btn-primary btn-lg btn-block"
							style="font-size: 15px;" name="submit" >
							<i class="fa fa-check-square"></i> Cadastrar
						</button>
					</div>
	
					<div class="col-md-6">
						<a href="index.php" class="btn btn-default btn-lg btn-block"
							style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
						</a>
					</div>
				</div>
		</form>
	</div>
</div>

<?php
include FOOTER;
?>