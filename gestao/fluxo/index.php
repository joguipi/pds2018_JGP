<?php
require_once 'functions.php';
index();
require_once HEADER;
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Entrada e Saída
		</h1>
	</div>
</div>
			
<div class="row">
	<div class="col-md-3">
		<a href="add.php" class="btn btn-primary btn-md">
			<i class="fa fa-plus-square" aria-hidden="true"></i> Registrar Entrada
		</a>
	</div>
</div>

<br/>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Tabela com todos os registros de entrada e saída</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<table width="100%"
					class="table table-striped table-bordered table-hover"
					id="dataTables-example">
					<thead>
						<tr>
							<th>Dia e Hora - Entrada</th>
							<th>Nome do Visitante</th>
							<th>Veículo do Visitante</th>
							<th>OPÇÕES</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							if (count ($fluxos) > 0)
							{
								foreach ($fluxos as $fluxo)
								{
									$visitante = find_id('tbl_visitantes', 'IDVisitante', $fluxo['IDVisitante']);
									$veiculo = find_id('tbl_veiculos', 'IDVeiculo', $visitante[0]['IDVeiculo']);
									$date = new DateTime($fluxo['HorarioEntrada']);
						?>
									<tr class="odd gradeX">
										<td><?php echo $date->format('d/m/Y H:i:s');?></td>
										<td> <?php echo $visitante[0]['Nome']?> </td>
										<td> <?php if(count($veiculo) > 1) echo 'Sem Registro';
											else echo $veiculo[0]['Placa'];?> </td>
												<td>
													<a href="view.php?id=<?php echo $fluxo['IDFluxo'];?>" class="label label-primary">Ver</a>
										    		<?php 
										    		if($fluxo['HorarioSaida'] == null) {
										    		?>
										    		<a href="atualizar.saida.php?id=<?php echo $fluxo['IDFluxo'];?>" class="label label-default">Registrar Saída</a>
										    		<?php }?>
												</td>
									</tr>
						<?php 
								}
							}
						?>
					</tbody>
				</table>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<?php
require_once DELETEMODAL;
include FOOTER;
?>