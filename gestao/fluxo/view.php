<?php
require_once 'functions.php';
view($_GET['id']);
require_once HEADER;
$fluxos =  find_id('tbl_fluxo','IDFLuxo',$_GET['id']);
$visitante = find_id('tbl_visitantes', 'IDVisitante', $fluxos[0]['IDVisitante']);
$proprietario = find_id('tbl_proprietarios', 'IDProprietario', $fluxos[0]['IDProprietario']);
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Informações do Fluxo de Entrada e Saída
		</h1>
	</div>
</div>

<!-- Código abaixo auxilia para verificar o que está sendo retornado -->
<?php 
//	echo '<pre>';
//	print_r($lotes);
//	echo '</pre>';
?>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-12">
						<h4><strong>Detalhes:</strong></h4>
					</div>
				</div>
			</div>
			<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<p><strong>Nome do Visitante: </strong><?php echo $visitante[0]["Nome"]?></p>
				</div>
				
				<div class="col-md-4">
					<p><strong>Nome do Proprietário: </strong><?php echo $proprietario[0]["Nome"]?></p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<p><strong>Horário de Entrada: </strong><?php 
					$date = new DateTime($fluxos[0]["HorarioEntrada"]);
					echo $date->format('d/m/Y H:i:s');
					?></p>
				</div>
				
				<div class="col-md-4">
					<p><strong>Horário de Saída: </strong><?php 
					if($fluxos[0]["HorarioSaida"] == null) echo 'Não há registros';
					else {
						$date2 = new DateTime($fluxos[0]["HorarioSaida"]);
						echo $date2->format('d/m/Y H:i:s');
					}?></p>
				</div>
			</div>
			
			</div>
		</div>
	</div>
</div>

<?php
require_once FOOTER;
?>