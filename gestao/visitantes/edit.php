<?php
include 'functions.php';


edit($_GET['id']);

include HEADER;

$visitante = find_id("tbl_visitantes", "IDVisitante" , $_GET['id']);
$carro = find('tbl_veiculos', 'IDVeiculo', $visitante[0]['IDVeiculo']);
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Editar Visitante
		</h1>
	</div>
</div>

<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<form action="edit.php?id=<?php echo $_GET['id'];?>" method="post" >
								
			<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Nome</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o nome completo"></i>
							<input type="text" class="form-control" id="campo_nome" name="visitante[Nome]" value="<?php echo $visitante[0]['Nome']?>" required autofocus/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">CPF</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o CPF do proprietário"></i>
							<input type="text" class="form-control" id="campo_nome" name="visitante[CPF]" value="<?php echo $visitante[0]['CPF']?>"required autofocus/>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div id="div_campo_nome" class="form-group has-feedback">
							<label for="campo_nome">Placa</label>
							<i class="fa fa-1-5x fa-question-circle-o pull-right" data-toggle="tooltip" data-placement="left" title="Deve ser informado o ID do Veículo"></i>
							<input type="text" class="form-control" id="campo_nome" name="visitante[Placa]" value="<?php if ($visitante[0]['IDVeiculo'] != '') echo $carro[0]['Placa']?>" autofocus/>
						</div>
					</div>
				</div>
			
			<div class="row">
				<div class="col-md-6">
					<button type="submit" id="btn_submit" class="btn btn-primary btn-lg btn-block"
						style="font-size: 15px;" name="submit" >
					<i class="fa fa-check-square"></i> Editar
				</button>
			</div>
			
			<div class="col-md-6">
				<a href="index.php" class="btn btn-default btn-lg btn-block"
					style="font-size: 15px;"> <i class="fa fa-times"></i> Cancelar
					</a>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- /.row (nested) -->

<?php
include FOOTER;
?>