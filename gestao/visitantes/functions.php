<?php
require_once '../config.php';
function index() {
	global $visitantes;
	$visitantes = find_all ( 'TBL_VISITANTES' );
}
function view($id) {
	if (isset ( $id )) {
		global $visitante;
		$result = find_id ( 'TBL_VISITANTES', 'IDVisitante', $id );
		if (count ( $result ) == 0) {
			$_SESSION ['message'] = "Não foi possível visualizar: ID não encontrado";
			$_SESSION ['type'] = 'danger';
			header ( 'Location: index.php' );
			exit ();
		}
		$perfil = $result [0];
	} else {
		$_SESSION ['message'] = "Não foi possível visualizar: ID não especificado";
		$_SESSION ['type'] = 'danger';
		header ( 'Location: index.php' ); //
		exit ();
	}
}
function add() {
	if (isset ( $_POST ['submit'] )) {
		$visitante = $_POST ['visitante'];
				
		// Verifica se o CPF é válido.
		if (validaCPF ( $visitante ['CPF'] ) == false) {
			$_SESSION ['message'] = "CPF Inválido";
			$_SESSION ['type'] = 'danger';
			header ( 'Location: index.php' );
			exit ();
		}
		
		// Verifica se possuí placa.
		if ($visitante ['Placa'] != "") {
			// Procura pela placa digitada:
			$carro = find ( 'tbl_veiculos', 'Placa', $visitante ['Placa'] );
			if (count ( $carro ) > 0) {
				$visitante ['IDVeiculo'] = $carro [0] ['IDVeiculo'];
			} else {
				$_SESSION ['message'] = "Placa de Veículo não encontrado";
				$_SESSION ['type'] = 'danger';
				header ( 'Location: index.php' );
				exit ();
			}
		}
		// Remove a Placa
		unset ( $visitante ['Placa'] );
		$visitante['CPF'] = preg_replace ( "/[^0-9]/", "", $visitante['CPF']);
		
		save ( 'TBL_VISITANTES', $visitante );
		if ($_SESSION ['type'] == "success") {
			$_SESSION ['message'] = "Visitante cadastrado com sucesso";
			header ( 'Location: index.php' );
			exit ();
		} else {
			$_SESSION ['message'] = "Não foi possível cadastrar";
			$_SESSION ['type'] = 'danger';
			header ( 'Location: index.php' );
			exit ();
		}
	}
}
function edit($id) {
	if (isset ( $id )) {
		if (isset ( $_POST ['submit'] )) {
			
			$visitante = $_POST ['visitante'];
			$visitante ['IDVeiculo'] = null;
			
			// Verifica se possuí placa.
			if ($visitante ['Placa'] != "") {
				// Procura pela placa digitada:
				$carro = find ( 'tbl_veiculos', 'Placa', $visitante ['Placa'] );
				if (count ( $carro ) > 0) {
					$visitante ['IDVeiculo'] = $carro [0] ['IDVeiculo'];
				} else {
					$_SESSION ['message'] = "Placa de Veículo não encontrado";
					$_SESSION ['type'] = 'danger';
					header ( 'Location: index.php' );
					exit ();
				}
			}
			// Remove a Placa
			unset ( $visitante ['Placa'] );
			
			if (validaCPF ( $visitante ['CPF'] ) == true) {
				$visitante['CPF'] = preg_replace ( "/[^0-9]/", "", $visitante['CPF']);
				update ( "tbl_visitantes", $visitante, "IDVisitante", $id );
				
				if ($_SESSION ['type'] == "success") {
					$_SESSION ['message'] = "Visitante editado com sucesso";
					header ( 'Location: index.php' );
					exit ();
				}
			} else {
				$_SESSION ['message'] = "CPF Inválido";
				$_SESSION ['type'] = 'danger';
				header ( 'Location: index.php' );
				exit ();
			}
		} else {
			view ( $id );
		}
	} else {
		$_SESSION ['message'] = "Não foi possível visualizar: ID não especificado";
		$_SESSION ['type'] = 'danger';
		header ( 'Location: index.php' ); //
		exit ();
	}
}
function delete($id) {
	if (isset ( $id )) {
		remove ( 'TBL_VISITANTES', 'IDVisitante', $id );
		if ($_SESSION ['type'] == "success") {
			$_SESSION ['message'] = "Visitante excluido com sucesso";
			header ( 'Location: index.php' );
			exit ();
		} else {
			$_SESSION ['type'] = "danger";
			$_SESSION ['message'] = "Não é possível deletar. Há dados associados a este perfil";
			header ( 'Location: index.php' );
			exit ();
		}
	} else {
		$_SESSION ['message'] = "Não foi possível deletar: ID não especificado";
		$_SESSION ['type'] = 'danger';
		header ( 'Location: index.php' );
		exit ();
	}
}
function validaCPF($cpf = null) {
	
	// Verifica se um número foi informado
	if (empty ( $cpf )) {
		return false;
	}
	
	// Elimina possivel mascara
	$cpf = preg_replace ( "/[^0-9]/", "", $cpf );
	$cpf = str_pad ( $cpf, 11, '0', STR_PAD_LEFT );
	
	// Verifica se o numero de digitos informados é igual a 11
	if (strlen ( $cpf ) != 11) {
		return false;
	} // Verifica se nenhuma das sequências invalidas abaixo
	  // foi digitada. Caso afirmativo, retorna falso
	else if ($cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
		return false;
		// Calcula os digitos verificadores para verificar se o
		// CPF é válido
	} else {
		
		for($t = 9; $t < 11; $t ++) {
			
			for($d = 0, $c = 0; $c < $t; $c ++) {
				$d += $cpf {$c} * (($t + 1) - $c);
			}
			$d = ((10 * $d) % 11) % 10;
			if ($cpf {$c} != $d) {
				return false;
			}
		}
		
		return true;
	}
}
function mask($val, $mask) {
	$maskared = '';
	$k = 0;
	for($i = 0; $i <= strlen ( $mask ) - 1; $i ++) {
		if ($mask [$i] == '#') {
			if (isset ( $val [$k] ))
				$maskared .= $val [$k ++];
		} else {
			if (isset ( $mask [$i] ))
				$maskared .= $mask [$i];
		}
	}
	return $maskared;
}

?>