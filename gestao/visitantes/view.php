<?php
require_once 'functions.php';
view($_GET['id']);
require_once HEADER;
$visitantes =  find_id('tbl_visitantes','IDVisitante',$_GET['id']);
$carro = find('tbl_veiculos', 'IDVeiculo', $visitantes[0]['IDVeiculo']);
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Informações do Visitante
		</h1>
	</div>
</div>

<!-- Código abaixo auxilia para verificar o que está sendo retornado -->
<?php 
//	echo '<pre>';
//	print_r($lotes);
//	echo '</pre>';
?>

<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-12">
						<h4><strong>Detalhes:</strong></h4>
					</div>
				</div>
			</div>
			<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<p><strong>Nome: </strong><?php echo $visitantes[0]["Nome"]?></p>
				</div>
				
				<div class="col-md-4">
					<p><strong>CPF: </strong><?php echo mask($visitantes[0]["CPF"], '###.###.###-##')?></p>
				</div>
				
			</div>
			<div class="row">
				<div class="col-md-4">
					<p><strong>Placa Veículo: </strong><?php
					if ($visitantes[0]['IDVeiculo'] == '') echo "Não cadastrado";
					else echo $carro[0]["Placa"]?></p>
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

<?php
require_once FOOTER;
?>