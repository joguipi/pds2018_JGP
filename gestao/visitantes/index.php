<?php
require_once 'functions.php';
index();

require_once HEADER;
?>

<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header nao_imprimir">
			Visitantes
		</h1>
	</div>
</div>
			
<div class="row">
	<div class="col-md-3">
		<a href="add.php" class="btn btn-primary btn-md">
			<i class="fa fa-plus-square" aria-hidden="true"></i> Adicionar novo Visitante
		</a>
	</div>
</div>

<br/>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">Tabela com todos os visitantes cadastrados no sistema</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
				<table width="100%"
					class="table table-striped table-bordered table-hover"
					id="dataTables-example">
					<thead>
						<tr>
							<th>Nome</th>
							<th>CPF</th>
							<th>OPÇÕES</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							if (count ($visitantes) > 0)
							{
								foreach ($visitantes as $visitante)
								{
						?>
									<tr class="odd gradeX">
										<td><?php echo $visitante['Nome']?></td>
										<td> <?php echo mask($visitante['CPF'], '###.###.###-##')?> </td>
												<td>
													<a href="view.php?id=<?php echo $visitante['IDVisitante'];?>" class="label label-primary">Ver</a>
													<a href="edit.php?id=<?php echo $visitante['IDVisitante'];?>" class="label label-warning">Editar</a>
										    		<a href="#" class="label label-danger" data-toggle="modal" data-target="#deleteModal" id="excluir_<?php echo $visitante['IDVisitante']?>" data-id="<?php echo $visitante['IDVisitante']?>" data-visitante="<?php echo $visitante['Nome']?>" data-tipo='VISITANTE'>Excluir</a>
												</td>
									</tr>
						<?php 
								}
							}
						?>
					</tbody>
				</table>
				<!-- /.table-responsive -->
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<?php
require_once DELETEMODAL;
include FOOTER;
?>