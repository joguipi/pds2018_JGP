<?php
/** caminho absoluto para a pasta do sistema **/
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/../');
	
/** caminho no server para o sistema **/
if($_SERVER['HTTP_HOST']=='grsystems.com.br' || $_SERVER['HTTP_HOST']=='localhost')
{
	if ( !defined('BASEURL') )
		define('BASEURL', '/cond/');
}
	
/** caminho do arquivo de banco de dados **/
if ( !defined('DBAPI') )
		define('DBAPI', ABSPATH . 'includes/database.php');
		
if ( !defined('CONNECTION_FOLDER') )
	define('CONNECTION_FOLDER', ABSPATH . 'connections/');

/** caminhos dos templates de header e footer **/
define('HEADER', ABSPATH. 'gestao/header.php');
define('FOOTER', ABSPATH. 'gestao/footer.php');
define('DELETEMODAL', ABSPATH. 'includes/delete_modal.html');

require_once DBAPI;	

session_start();

?>