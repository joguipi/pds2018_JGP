<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TLogger
 *
 * @author Eleandro
 * esta classe prove uma interface abstrata para definiçao de algoritmos de log
 */
abstract class TLogger {
    //put your code here
    protected $filename; //local do arquivo de LOG
    
    /*
     * metodo __construct()
     * instancia um logger
     * @param $filename = local do arquivo de log
     * 
     */
    
    public function __construct($filename) {
        $this->filename = $filename;
        //reseta o coteúdo do arquivo
        file_put_contents($filename, '');
    }
    
    //define o método write como obrigatório
    abstract function write($messagem);
}
?>