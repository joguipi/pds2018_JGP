<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TConnection
 *gerencia conexoes com bancos de dados através de arquivos de configuração
 * @author Eleandro
 */
final class TConnection {
    //put your code here
    
    /*metodo __construct()
     * não existirão instancias de TConnection, por isto estamos marcando-o como private
     */
    
    private function __construct() {
    
    }
    
    /*
     * metodo open()
     * recebe o nome do banco de dados e instancia o objeto PDO correspondente
     */
    
    public static function open($name)
    {
        //verifica se exite arquivo de configuração para este banco de dados
    	if (file_exists(CONNECTION_FOLDER."{$name}.ini"))
        {
            //lê INI e retorno um array
        	$db = parse_ini_file(CONNECTION_FOLDER."{$name}.ini");
        }
        else
        {
            //se não existir, lança um erro
            throw new Exception("Arquivo '$name' não encontrado");
        }
        
        //lê as informações contidas no arquivo
        $user = $db['user'];
        $pass = $db['pass'];
        $name = $db['name'];
        $host = $db['host'];
        $type = $db['type'];
        
        
        //descobre qual o tipo (driver) de banco de dados a ser utilizado
        
        switch ($type)
        {
            case 'pgsql':
                $conn = new PDO("pgsql:dbname={$name};user={$user}; password={$pass};host={$host}");
                break;
            case 'mysql':
                $conn = new PDO("mysql:host={$host};dbname={$name};charset=utf8",$user,$pass);
                break;
            case 'sqlite':
                $conn = new PDO("sqlite:{$name}");
                break;
            case 'ibase':
                $conn = new PDO("firebird:host={$host};dbname={$name};",$user, $pass);
                break;
            case 'ibase2':
                $conn = new PDO("interbase:dbname={$name}",$user, $pass);
                //echo $conn;
                break;            
            case 'oci8':
                $conn = new PDO("oci:dbname={$name}", $user, $pass);
                break;
            case 'pgsql':
                $conn = new PDO("mssql:host={$host},1433;dbname={$name}", $user, $pass);
                break;
        }
        //define para que o PDO lance exceções na ocorrencia de erros
        $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        
        //$conn->query('SET character_set_connection=utf8' );
        //$conn->query('SET character_set_client=utf8' );
        //$conn->query('SET character_set_results=utf8' );
        
        //RETORNA O OBJETO INSTANCIADO
        return $conn;
    }
}
?>