<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TSqlDelete
 *
 * @author Eleandro
 */
final class TSqlDelete extends TSqlInstruction{
    /*
     * metodo getInstruction()
     * retorna a instrução de DELETE em forma de string
     */
    
    public function getInstruction() {
        //monta string de DELETE
        $this->sql = "DELETE FROM {$this->entity}";
        
        //retorna a cláusula WHERE do objetvo $this->criteria
        if($this->criteria)
        {
            $expression = $this->criteria->dump();
            if ($expression)
            {
                $this->sql .= ' WHERE ' . $expression;
            }
        }
        return $this->sql;
    }
    
}
?>
