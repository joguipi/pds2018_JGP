<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TLoggerXML
 *
 * @author Eleandro
 */
class TLoggerXML extends TLogger {
    //put your code here
    /*
     * método write()
     * escreve uma mensagem no arquivo de log
     * @param $message = mensagem a ser escrita
     */
    
    public function write($message)
    {
        $time = date("Y-m-d H:i:s");
        //monta a string
        $text = "<log>\n";
        $text.=" <time>$time</time>\n";
        $text.=" <message>$message</message>\n";
        $text.="</log>\n";
        
        //adiciona ao final do arquivo
        
        $handler = fopen($this->filename,'a');
        fwrite($handler, $text);
        fclose($handler);
        
    }
}
?>