<?php
class Log
{
    
    private $filename;
    private $formato_horario;
    //private $tamanho;
    
    private function Fazer_backup()
    {
        
        $ler = fopen($this->filename.'.log', 'rb');
        $criar = $criar = fopen($this->filename.'.bak', 'wb');
        fwrite($criar, fread($ler, filesize($this->filename.'.log')));
        fclose($ler);
        fclose($criar);
        
        if (!copy($this->filename.'.log', $this->filename.'.bak')) {
            echo "falha ao copiar $this->filename...\n";
        }
        clearstatcache();
    }
    
    public function Log($filename, $formato_horario='d-m-Y H:i:s')
    {
        $this->filename = $filename;
        $this->formato_horario = $formato_horario;
        if(!file_exists($filename.'.log'))
        {
            if(file_exists($filename.'.bak'))
            {
                $ler = fopen($filename.'.bak', 'r');
				$string = fread($ler, filesize($filename.'.bak'));
				fclose($ler);
                $criar = fopen($filename.'.log', 'w');
                $this->escrever("$filename.log não existe. Tentando restaurar backup", "warning");
                try
                {
                    $this->escrever("Backup encontrado:");
					$this->escrever($string);
                    $this->escrever("Backup restaurado", "success");
                }
                catch (Exception $e)
                {
                    $this->escrever($e->getMessage(), "error");
                }
                
            }
            else 
            {
                $criar = fopen($filename.'.log', 'w');
                //$this->escrever("$filename.log não existe, $filename.bak não existe", "warning");
                $this->escrever("$filename.log não existe", "warning");
                $this->escrever("Criando novo arquivo");
                $this->escrever("Arquivo Criado", "success");
            }
            fclose($criar);
        }
        //$this->tamanho = filesize($this->filename.'.log');
        clearstatcache();
    }
    
    public function escrever($mensagem, $type="info")
    {
        date_default_timezone_set('America/Sao_Paulo');
        
        $this->Fazer_backup();
        
        $str = "[".date($this->formato_horario)."] :: [$type] - $mensagem".PHP_EOL;
        
        //$this->tamanho+=sizeof($str);
        
        $escrever = fopen($this->filename.'.log', 'a');
        fwrite($escrever, $str);
        fclose($escrever);
        clearstatcache();
    }
    
    public function imprimir()
    {
        $ler = fopen($this->filename.'.log', 'r');
        echo '<pre>'.fread($ler, filesize($this->filename.'.log')).'</pre>';
        fclose($ler);
        clearstatcache();
    }
}