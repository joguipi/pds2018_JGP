$(document).ready(function(){
	$('#dataTables-example').DataTable({
        responsive: true
    });
	
	
	$('#deleteModal').on('show.bs.modal', function (event)
	{
		var button = $(event.relatedTarget);
		var modal = $(this);
		var id = button.data('id');
		var tipo = button.data('tipo');
				
		if(tipo=="LOTE")
		{
			var lote = button.data('lote');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+lote+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O LOTE '+lote+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="VEICULO")
		{
			var veiculo = button.data('veiculo');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+veiculo+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O VEÍCULO '+veiculo+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="PROPRIETARIO")
		{
			var proprietario = button.data('proprietario');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+proprietario+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR O PROPRIETÁRIO '+proprietario+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="VISITANTE")
		{
			var visitante = button.data('visitante');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+visitante+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE O(A) VISITANTE(A) '+visitante+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		else if(tipo=="USUARIO")
		{
			var usuario = button.data('usuario');
			modal.find('.modal-title').html('<strong>Excluir #'+id+" - "+usuario+":</strong>");
			modal.find('.modal-body').html('<p><strong>DESEJA REALMENTE EXCLUIR ESSE USUARIO '+usuario+'</p></strong>');
			modal.find('#deleteBtn').attr('href', 'delete.php?id='+id);
		}
		
	});
	
});

